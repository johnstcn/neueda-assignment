# JSON-XML Conversion and Encryption/Decryption Service

This project consists of two services: `encryptsvc` and `decryptsvc` that convert JSON to XML, encrypt, decrypt, and serialize to disk.
For a short demonstration, the script `./run.sh` will build the containers, start the service (via `docker-compose`), send a sample request, and stop the service.
More details of each service are provided below.

## `encryptsvc`

`encryptsvc` is a small Flask application that listens for HTTP requests containing a JSON payload.
Upon receiving a HTTP request containing a valid JSON payload, it will convert the JSON to XML, encrypt it using a shared secret (see [below](#Encryption)), and send it via ZeroMQ to `decryptsvc`.

It can be controlled via the following environment variables:
  - `HTTP_HOST`: address on which to serve HTTP requests. By default, the service listens on all interfaces.
  - `HTTP_PORT`: port on which to serve HTTP requests. By default, the service listens on port 5000.
  - `ZMQ_HOST`: address on which to serve ZeroMQ. Listens on all interfaces by default.
  - `ZMQ_PORT`: address on which to serve ZeroMQ. Listens on port 5680 by default.
  - `SECRET_PATH`: path to shared secret to be used for encryption/decryption. Defaults to `/tmp/secretkey`. This must be exactly 32 bytes of random data.

## `decryptsvc`
`decryptsvc` fetches encrypted XML payloads from `encryptsvc` via ZeroMQ, decrypts them using the shared secret (see [below](#Encryption)), and persist it to disk in a configurable location.

It can be configured via the following environment variables:
  - `ZMQ_HOST`: ZMQ address of `encryptsvc`. Defaults to `localhost`.
  - `ZMQ_PORT`: ZMQ port of `encryptsvc`. Defaults to 5680.
  - `SECRET_PATH`: path to shared secret. Defaults to `/tmp/secretkey`. This must be exactly 32 bytes of random data, and should match that provided to `encryptsvc`.
  - `OUTPUT_BASE_PATH`: base directory in which to persist decrypted XML payloads recieved. Defaults to the current directory (`./`)


## Encryption
Encryption is performed using symmetric encryption, using [`PyNaCl`](https://pynacl.readthedocs.io/en/latest/) as an encryption library. At the time of writing, the algorithms used by `PyNaCl` are the `XSalsa20` stream cipher and `Poly1305` message authentication code. Random nonces are added to each encrypted payload.

The shared secret is required to be 32 bytes long. Note that this shared secret is currently **not** put through a key derivation function. Also note that this does **not** provide perfect forward secrecy. If an attacker manages to gain possession of the shared secret, they will be able to decrypt any intercepted payloads.


## JSON-XML conversion

JSON is converted to XML using the following mapping rules:
  - All JSON datatypes map to a `<value>` element.
  - JSON objects map to `<value type="object">`. Such values contain a number of `<key>` elements, with a `name` attribute matching the original attribute name. Each `<key>` element contains a correspondin `<value>` element.
  - JSON arrays map to multiple `<value>` elements.
  - JSON strings map to `<value type="string">`. The string contents are enclosed in a `CDATA` element.
  - JSON numeric types map to `<value type="numeber">`.
  - JSON boolean types map to `<value type="boolean">`.
  - JSON `null` types map to `<value type="null"/>`.

For an example, consider the following JSON document:
```json
{
    "a": 1,
    "b": ["c", 2],
    "d": {"e": True},
    "f": None,
    "g": "happy face 😊",
    "h": 3.14
}
```

This will map to the following XML document:
```xml
<value type="object">
    <key name="a">
        <value type="number">1</value>
        </key>
    <key name="b">
        <value type="array">
            <value type="string"><![CDATA[c]]></value>
            <value type="number">2</value>
        </value>
    </key>
    <key name="d">
        <value type="object">
            <key name="e">
                <value type="boolean">true</value>
            </key>
        </value>
    </key>
    <key name="f">
        <value type="null"/>
    </key>
    <key name="g">
        <value type="string"><![CDATA[happy face &#128522;]]></value>
    </key>
    <key name="h">
        <value type="number">3.14</value>
    </key>
</value>
```