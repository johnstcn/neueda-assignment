FROM python:3.9.0-slim-buster

COPY requirements-decryptsvc.txt /tmp/
RUN pip install -r /tmp/requirements-decryptsvc.txt

RUN useradd --create-home decryptsvc -d /var/lib/decryptsvc
COPY src/decryptsvc.py src/secret.py /var/lib/decryptsvc/

WORKDIR /var/lib/decryptsvc
USER decryptsvc

CMD ["/var/lib/decryptsvc/decryptsvc.py"]