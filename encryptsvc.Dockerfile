FROM python:3.9.0-slim-buster

RUN apt-get update -y && \
    apt-get install -y --no-install-recommends \
        curl \
        gcc \
        libxml2-dev \
        libxslt-dev \
        zlib1g-dev \
        python3-dev

COPY requirements-encryptsvc.txt /tmp/
RUN pip3 install -r /tmp/requirements-encryptsvc.txt

RUN apt-get remove -y \
        gcc \
        libxml2-dev \
        libxslt-dev \
        zlib1g-dev \
        python3-dev
RUN apt-get clean -y

RUN useradd --create-home encryptsvc -d /var/lib/encryptsvc -u 12345
COPY src/encryptsvc.py src/util.py src/secret.py /var/lib/encryptsvc/
RUN chown -R 12345:12345 /var/lib/encryptsvc/*

WORKDIR /var/lib/encryptsvc
USER encryptsvc

EXPOSE 5000/tcp
HEALTHCHECK --interval=1s --retries=3 --timeout=10s CMD curl --fail http://localhost:5000/ || exit 1
CMD ["/usr/local/bin/gunicorn", "--bind", "0.0.0.0:5000", "encryptsvc:app"]