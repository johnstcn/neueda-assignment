#!/usr/bin/env bash

set -euo pipefail
set -x

if [[ ! -f ./secret ]]; then
    dd if=/dev/urandom of=./secret bs=32 count=1
fi

if [[ ! -d ./data ]]; then
    mkdir -p ./data
fi

# clean up old files
rm -vf data/*.xml

docker-compose up --build --detach

sleep 3 # HACK: the first response from gunicorn is empty unless we wait 

curl -XPUT http://localhost:5000 --data '{"foo":"bar"}' -H 'Content-Type: application/json'

docker-compose down -t 0

echo "contents of ./data directory:"
ls -lart ./data