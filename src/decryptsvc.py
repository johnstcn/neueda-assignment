#!/usr/bin/env python3

import hashlib
import os
import signal
import zmq

from secret import SecretHelper

SECRETHELPER = None
RUN = True


def handle_shutdown(signum, frame):
    """
    handler for shutdown signals
    """
    print(f"got signal {signum} from {frame}")
    global RUN
    RUN = False


def main():
    zmq_host = os.getenv("ZMQ_HOST", "127.0.0.1")
    zmq_port = int(os.getenv("ZMQ_PORT", "5680"))
    output_base_path = os.getenv("OUTPUT_BASE_PATH", ".")
    secret_path = os.getenv("SECRET_PATH", "/tmp/secretkey")

    global SECRETHELPER
    try:
        # TODO: implement key rotation
        with open(secret_path, "rb") as f:
            SECRETHELPER = SecretHelper(f.read())
    except FileNotFoundError:
        print(f"shared secret {secret_path} not found, exiting")
        raise SystemExit

    zmq_ctx = zmq.Context()
    zmq_sock = zmq_ctx.socket(zmq.PULL)
    zmq_sock.connect(f"tcp://{zmq_host}:{zmq_port}")

    signal.signal(signal.SIGINT, handle_shutdown)
    signal.signal(signal.SIGTERM, handle_shutdown)

    while RUN:
        try:
            encrypted_message = zmq_sock.recv()
            plaintext_message = SECRETHELPER.decrypt(encrypted_message)
            msg_hash = hashlib.sha256(plaintext_message).hexdigest()
            filename = f"{msg_hash}.xml"
            file_path = os.path.join(output_base_path, filename)
            with open(file_path, "wb") as f:
                f.write(plaintext_message)
            print(f"wrote {len(plaintext_message)} bytes to {file_path}")
        except KeyboardInterrupt:
            print("got keyboard interrupt, exiting")
            raise SystemExit


if __name__ == "__main__":
    main()
