#!/usr/bin/env python3

import os

from flask import Flask, request, abort, jsonify, make_response
import nacl.exceptions
import zmq

from secret import SecretHelper
from util import obj2xml

app = Flask(__name__)
SECRETHELPER = None
ZMQ_CTX = zmq.Context()
ZMQ_SOCK = None

@app.route("/", methods=['GET'])
def handle_index_get():
    return make_response(jsonify(message="send me a PUT request containing JSON"), 200)


@app.route("/", methods=['PUT'])
def handle_index_put():
    if not request.json:
        abort(make_response(jsonify(message="must supply a JSON document"), 400))
    xml_bytes = None
    try:
        xml_bytes = obj2xml(request.json)
    except ValueError as e:
        err_msg = "unable to convert JSON to XML: " + str(e)
        abort(make_response(jsonify(message=err_msg), 400))
        return

    encrypted_bytes = SECRETHELPER.encrypt(xml_bytes)
    # TODO: send async
    ZMQ_SOCK.send(encrypted_bytes)
    return make_response(jsonify(message="ok"), 200)


def setup():
    """
    initializes zmq and nacl
    """
    zmq_host = os.getenv("ZMQ_HOST", "127.0.0.1")
    zmq_port = int(os.getenv("ZMQ_PORT", "5680"))
    secret_path = os.getenv("SECRET_PATH", "/tmp/secretkey")

    global ZMQ_SOCK
    ZMQ_SOCK = ZMQ_CTX.socket(zmq.PUSH)
    print(f"connecting to zmq on tcp://{zmq_host}:{zmq_port}")
    ZMQ_SOCK.bind(f"tcp://{zmq_host}:{zmq_port}")

    global SECRETHELPER
    try:
        # TODO: implement key rotation
        with open(secret_path, "rb") as f:
            SECRETHELPER = SecretHelper(f.read())
    except FileNotFoundError:
        print(f"shared secret {secret_path} not found, exiting")
        raise SystemExit
    except nacl.exceptions.TypeError as e:
        print(f"invalid data in {secret_path}: {e}")
        raise SystemExit

# we need to ensure this is called when gunicorn loads the module
setup()

if __name__ == "__main__":
    http_host = os.getenv("HTTP_HOST", default="127.0.0.1")
    http_port = int(os.getenv("HTTP_PORT", default="5000"))
    app.run(host=http_host, port=http_port)
