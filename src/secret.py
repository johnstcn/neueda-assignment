#!/usr/bin/env python3

import nacl.secret
import nacl.utils

class SecretHelper(object):
    """
    SecretHelper is a wrapper for PyNaCl's SecretBox.

    >>> skey = bytes('0' * 32, 'ascii')
    >>> sh = SecretHelper(skey)
    >>> sh.decrypt(sh.encrypt(b'testing'))
    b'testing'
    >>> sh.decrypt(sh.encrypt('testing'))
    b'testing'
    """
    def __init__(self, skey: bytes):
        self._box = nacl.secret.SecretBox(skey)

    def encrypt(self, msg) -> bytes:
        # TODO: add message padding
        if isinstance(msg, str):
            return self._box.encrypt(bytes(msg, 'utf-8'))
        elif isinstance(msg, bytes):
            return self._box.encrypt(msg)
        else:
            return ValueError('msg must be either bytes or string')

    def decrypt(self, msg: bytes) -> bytes:
        # TODO: handle padded messages
        return self._box.decrypt(msg)
