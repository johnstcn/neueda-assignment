#!/usr/bin/env python3

import lxml.etree as et

def is_number(obj):
    """
    is_number returns True if obj is an integer or float, and False otherwise.

    >>> is_number(1)
    True

    >>> is_number(3.14)
    True

    >>> is_number(1+1j)
    False

    >>> is_number("foo")
    False
    """
    return isinstance(obj, int) or isinstance(obj, float)


def is_bool(obj):
    """
    is_bool returns True if obj is a boolean.

    >>> is_bool(True)
    True

    >>> is_bool(False)
    True

    >>> is_bool(0)
    False

    >>> is_bool(1)
    False
    """
    return obj is True or obj is False


def obj2xml(obj) -> bytes:
    """
    obj2xml converts the given object to an XML element.
    The object may consist (or contain) any of the following types which are representable natively in JSON: 
        - dict
        - list
        - str
        - int
        - float
        - bool
        - NoneType
    Other types will raise a ValueError.

    >>> complex_obj = {"a": 1, "b": ["c", 2], "d": {"e": True}, "f": None, "g": "happy face 😊", "h": 3.14}
    >>> obj2xml(complex_obj)
    b'<value type="object"><key name="a"><value type="number">1</value></key><key name="b"><value type="array"><value type="string"><![CDATA[c]]></value><value type="number">2</value></value></key><key name="d"><value type="object"><key name="e"><value type="boolean">true</value></key></value></key><key name="f"><value type="null"/></key><key name="g"><value type="string"><![CDATA[happy face &#128522;]]></value></key><key name="h"><value type="number">3.14</value></key></value>'

    >>> obj2xml({"unsupported_complex": 1+2j})
    Traceback (most recent call last):
        ...
    ValueError: unhandled JSON value: (1+2j)

    >>> obj2xml({"unsupported_bytes": b'some bytes'})
    Traceback (most recent call last):
        ...
    ValueError: unhandled JSON value: b'some bytes'

    """
    return et.tostring(__obj2xml(obj))


def __obj2xml(obj) -> et.Element:
    elem = et.Element("value")
    if isinstance(obj, dict):
        elem.set("type", "object")
        for k, v in obj.items():
            # handle key-value pair
            elem_key = et.Element("key")
            elem_key.set("name", k)
            elem_key.append(__obj2xml(v))
            elem.append(elem_key)            
        return elem
    elif isinstance(obj, list):
        elem.set("type", "array")
        for item in obj:
            # handle item
            elem.append(__obj2xml(item))
        return elem
    elif isinstance(obj, str):
        # handle str
        elem.set("type", "string")
        # lxml natively handles encoding
        elem.text = et.CDATA(obj)
        return elem
    elif is_bool(obj):
        # handle booleans - this must happen first because True and False are stored
        # internally as 1 and 0, respectively.
        elem.set("type", "boolean")
        elem.text = "true" if obj else "false"
        return elem
    elif is_number(obj):
        # handle numeric types
        elem.set("type", "number")
        elem.text = f"{obj}"
        return elem
    elif obj is None:
        # handle null
        elem.set("type", "null")
        return elem
    else:
        raise ValueError(f"unhandled JSON value: {obj}")
